import { configureStore, createReducer } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { createWrapper } from 'next-redux-wrapper';

export const api = createApi({
  baseQuery: fetchBaseQuery({ baseUrl: 'https://jsonplaceholder.typicode.com' }),
  endpoints: (build) => ({
    getPosts: build.query({
      query: () => '/posts',
    })
  })
});

export function makeStore() {
  return configureStore({
    reducer: {
      [api.reducerPath]: api.reducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat([api.middleware]),
    devTools: true,
  });
}

const store = makeStore();
setupListeners(store.dispatch);

export const wrapper = createWrapper(makeStore, { debug: false });

export default store;
